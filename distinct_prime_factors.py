import math
import time
start_time = time.time()

def primeFactors(n): 
    prime_factors = []
    while n % 2 == 0: 
        prime_factors.append(2)
        n = n / 2
    for i in range(3,int(math.sqrt(n))+1,2): 
        while n % i== 0: 
            prime_factors.append(i) 
            n = n / i 
    if n > 2: 
        prime_factors.append(n)
    return set(prime_factors)

def find_distinct_prime_factors(max_limit):
    i = 644
    while True:
        if len(primeFactors(i)) == 4 and len(primeFactors(i-1)) == 4 \
            and len(primeFactors(i-2)) == 4 and len(primeFactors(i-3)) == 4: 
            return i - 3
        i+=1

print(find_distinct_prime_factors(200))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )